# Oficial guide

Oficial guide can be found on [MySQL website](https://dev.mysql.com/doc/connector-cpp/8.1/en/).

Why I do this? Because [AUR package](https://aur.archlinux.org/packages/mysql-connector-c++) did not woked. 
The source webiste probaby stoped shareing the file.

# Download

First create dir somewhere. At the end you can remoove the whole dir.

```bash
$ mkdir ~/mysql-connector-cpp
```

Go to that directory.

```bash
$ cd ~/mysql-connector-cpp
```

Clone the repository from Git.

```bash
$ git clone https://github.com/mysql/mysql-connector-cpp.git
```

Go to the cloned directory.

```bash
$ cd mysql-connector-cpp
```

Check that you are in the correct branch.

```bash
$ git checkout 8.0
```

Go back to the main directory

```bash
$ cd ..
```


# Build

Create a separe build directory.

```bash
$ mkdir build
```

And go to the new directory.

```bash
$ cd build
```

Prepare for build.

```bash
$ cmake ../mysql-connector-cpp/
```

Build the project.

```bash
$ cmake --build .
```

Install the project. This will install it to `/usr/local/msql/` directory.

```bash
$ cmake --build . --target install
```

Go back to the main folder.

```bash
$ cd ..
```

# Testapp

*Note: You need to have a user witouth password in your database. Otherwise you wont be albe to log in.*

Make new directory.

```bash
$ mkdir buildtest
```

Go to that directory.

```bash
$ cd testapp
```

Prepare for build of test app.

```bash
$ cmake [other_options] -DWITH_CONCPP=concpp_install concpp_source/testapp
```

*Note: You have to delete the `[other_options]` part.*

*Note: `concpp_source` is the directory containing the Connector/C++ source code.*

*Note: `concpp_install` is the directory where Connector/C++ is installed.*

Example:

```bash
$ cmake -DWITH_CONCPP=/usr/local/mysql/connector-c++-8.0.33/ ../mysql-connector-cpp/testapp/
```

Build the project.

```bash
$ cmake --build .
```

Run the testapp.

```bash
$ run/devapi_test mysqlx://root@10.0.0.58
```

My output:

```
Creating session on mysqlx://test@10.0.0.58 ...
Session accepted, creating collection...
ERROR: CDK Error: Access denied for user 'test'@'%' to database 'test'
```


# Test

To test the compilation you can temporarly add the library to path.

```bash
$ export LD_LIBRARY_PATH=/usr/local/mysql/connector-c++-8.0.33/lib64/debug/
```

Then try to compile.

```bash
$ clang++ -std=c++17 -I /usr/local/mysql/connector-c++-8.0.33/include -L /usr/local/mysql/connector-c++-8.0.33/lib64/debug main.cpp -lmysqlcppconn8
```

# Add permanentley

To add the library permanentley so you can start the program you can add its path to 
`/etc/ld.so.conf.d/` directory as a new file.

```bash
$ sudo nano /etc/ld.so.conf.d/mysql-connector-cpp.conf
```

Then add the directory of the library here.

```
/usr/local/mysql/connector-c++-8.0.33/lib64/debug
```

You have to reboot to load the library.

```bash
$ reboot
```

Now this compilation should work.

```bash
$ clang++ -std=c++17 -I /usr/local/mysql/connector-c++-8.0.33/include -L /usr/local/mysql/connector-c++-8.0.33/lib64/debug main.cpp -lmysqlcppconn8
```
