# Download 

Cube programmer is availabe througt AUR package.

```bash
$ yay -S stm32cubeprogrammer
```

It will show you the configuration wizard. Just go trough it.

# Rules

You need to set udev rules. Those are shiped by STM. You can found them in the directory
you choosed in the wizard. For me it's `~/STMicroelectronics/STM32Cube/STM32CubeProgrammer/Drivers/rules/`.

You can copy the whole directiory with following command.

```bash
$ sudo cp STMicroelectronics/STM32Cube/STM32CubeProgrammer/Drivers/rules/*.* /etc/udev/rules.d/
```

This will copy everything so you should remove the readme and version text files. 

```bash
$ sudo rm /etc/udev/rules.d/Readme.txt 
```

```bash
$ sudo rm /etc/udev/rules.d/version.txt 
```

*Note: You can leave the text files in the udev folder.*


# How to flash

After opening the CubeProgrammer you should look on the right side. There are connect options.
When you select UART and the IC is in normal mode you should see something like `ttyACM0`.

![](images/CubeProgNorm.png)

When the IC is in BOOT mode you should select USB option and after refresh you should see 
something like `USB1`.

![](images/CubeProgBoot.png)

*Note: You have to click on the refresh button every time.*

With successfully connected IC you can choose firmware and upload.

![](images/CubeProgFirm.png)


