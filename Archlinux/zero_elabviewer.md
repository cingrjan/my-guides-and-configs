This guide is written for zero elabviewer v0.6

# Download

You can downlaod it from [GitHub](https://github.com/adamberlinger/zero_elabviewer/releases/tag/v0.6).

After download go to the folder with it (probaby `Downloads` folder).

```bash
$ cd Downloads
```

# Unzip

You have to unzip the zip file.

```bash
$ unzip zero_elabviewer_v0.6_linux.zip
```

# Serial port access

You need to have access to serial ports. 

*Note: if your distro is not listed just start a new issue and put "how to" information along with some reference into it.*

## Archlinux

In my case (Archlinux) I had to add my user to `uucp` group. You can do 
this with this command:

```bash
$ usermod -aG uucp [user]
```

*Note: Replace the `[user]` with your username (for exaple: HEX).*

More info about `uucp` group can be found [here](https://wiki.archlinux.org/title/users_and_groups#User_groups).

## Ubuntu

Ubuntu creates `plugdev` group. You should get access by adding your user to this group.

```bash
$ usermod -aG plugdev [user]
```

*Note: Replace the `[user]` with your username (for exaple: HEX).*

## Probably working udev rule

Create new rule file in `/etc/udev/rules.d/`. For example:

```bash
# nano /etc/udev/rules.d/55-usbs.rules
```

Then copy and paste following text into this file.

```
KERNEL=="ttyUSB[0-9]*",MODE="0666"
KERNEL=="ttyACM[0-9]*",MODE="0666"
```

This should allow regular users to use `ttyUSB?` and `ttyACM?` ports.

# Run

First go to the unziped folder.

```bash
$ cd zero_elabviewer_v0.6
```

Then you have to run the `run.sh` script.

```bash
$ ./run.sh
```

If everything went correctly the program should open. After plugging the IC to the USB port you should see it (you will probably
need to click on refresh button). In my case I see `ttyACM0` and I can connect to it.

