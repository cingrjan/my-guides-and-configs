# Symbol

This is the symbol of the processor. You will use pins 11 to 14.

![](.images/TSSOP20.png)

This is equivalent to the following image.

![](.images/STM32F042F6.png)

The symbol in schematic is just a fancy image for real word thing. It should 
tell you everything relevant about the part. 

In this case you can see that the pin number in schematic symbol matches the 
pin number on the PCB with the IC. This will be important! 

## PWM output

PWM output is pin 14 in both modes.

## Voltmeter

Voltmeter inputs are pins 11, 12 and 13. 

![](.images/TSSOP20-Voltmerer_mode.png)

*Note: You have to be in voltmerer mode.*

## Osciloscope

Osciloscope inputs are pins 11, 12 and 13.

![](.images/TSSOP20-Osc_mode.png)

*Note: You have to be in osciloscope mode.*

# Power suply, reset and boot

The **power pins** are pins **15** and **16**. YOU SHLOUD **CHECK YOUR CONNECTIONS** BEFORE 
TURNIN THE POWER ON. Every mistake will destroy the IC.

The **RESET** pin will reset the IC. It's the same as if you would disconnect and reconnect 
the USB input cable. It is located on pin number **4**.

The **BOOT** pin can tell the IC to go to BOOT mode. It is loacted on pin **1**.
The IC will check state of this pin
on vevery startup. If you want to go to BOOT mode you have to hold this button while 
pressing the RESET button. 

![](.images/TSSOP20-pinout.png)
