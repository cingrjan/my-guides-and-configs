# Start example

Just look at this chunkey boy. How much do you understand? Allmost nothing, right? I will try to change that.

![](.images/kit-schematic.png)

# How to read schematics

The schematic is just a picture that should show you how to connect certain parts togeather. If you take a look
at the example previous example you can see that it contains some symbols and some lines.

Let's start with the **lines**. Each line represents ideal connection. In real world this mean some kind of 
conductor (for example a **wire** or PCB trace). 
In the schematic you can see that the pin 18 of the IC is directly
connected to the USB port. This means that you have to take a wire and connect these pins on the breadbord.

Next we will take a look at the **symbols**. Each symbol represent some kind of **physical componnent** (for 
example capacitor, resistor or LED). Each symbol will have some connections. This means that you have to 
connect the part with conductors (wires) according to the lines.

# Schematic symbols

This is a list of all symbols that you can encounter during the course.

*Note: The list is not completed yet.*


## Resistor

Resistor is a basic electronic component. 

### Symbol

Standard **Europian** symbol looks like this:

![](.images/Resistor/symbol-resistor.png.png)

**American** symbol:

![](.images/Resistor/symbol-resistor-american.png)

### Physical part

![](.images/Resistor/resistor-blue.jpg)

![](.images/Resistor/resistor-yellow.jpg)

### Values

Every resistor can have different vaule. To distinguish between them they have color stips.
Last strip means tolerance. This is not that impornat. Strip before that is ratio. This 
means how many zeros you should add at the end of the number. All remaining strips are 
just numbers.

![](.images/Resistor/color-code.png)

If you get resistor with colors `BROWN BLACK BLACK RED` it means `1 0 0 2` and this means 
`10 000 Ohms` or `10 kOhms`.


## Capacitor

There are **two tpes** of capcaitors. Polarized and not polarized. 

**Polarized** capacitor is the capacitor that has has defined plus and minus poles. 
This means it **cannot be inserted backwords!** Any backwords connection will lead
to it's destruction.

### Symbol

![](.images/Capacitor/symbols-cap.jpg)

### Physical part

*Note: If the cap is polarized it will be noted after the name.*

- **Electrolythic capacitor** *(polarized)*

![](.images/Capacitor/electrolyth.png)

- **Polyester capacitor**

![](.images/Capacitor/polyester.png)

- **Ceramics capacitor**

![](.images/Capacitor/ceramics.png)


## Inductor


## Diode

This is a diode that is unable to emmit light.

This component has a `cathode` and `anode`. When current flows thourh the diode from `anode` to 
`cathode`. If you place it backword nothing will happen (on such a low voltage 
lika a 3.3 or 5 Volts).

### Symbol

![](.images/diode/symbol-diode.png)

### Physical part

![](.images/diode/diode.png)


## LED

This is a diode that can emmit light. It can bee visible light (for example red, green or blue) or 
other light that cannot be seen by human eye.

This component has a `cathode` and `anode`. When current flows thourh the diode from `anode` to 
`cathode` the LED lights up. If you place it backword nothing will happen (on such a low voltage 
lika a 3.3 or 5 Volts).

### Symbol

![](.images/LED/symbol-led.png)

### Physical part

![](.images/LED/leds.png)


## Button

Button can have a lot symbolst. It sometimes takes a bit of thinking.
Most of the symbols should look like a two dots and they will be connected by some 
line. The line will look like letter T. You can imagine that when the button is pressed 
at the top it will connect the two dots with the line betwee (or on top of) them.

### Symbol

![](.images/button/symbol-button.png)

### Physical part

![](.images/button/micro-button.png)


## USB port PCB

In this this is just a simple circuit board with USB mini connector on it.

### Symbol

![](.images/USB-port/symbol-usb-port.png)

### Physical part

![](.images/USB-port/port.png)


## Regulator

This component will create a stable voltage from some input voltage (in this case you will make 3.3 Volts
out of 5 Volts). You should **double check your connection**, because you can hurt the regulator.

### Symbol

![](.images/regulator/symbol-regul.png)

### Physical part

![](.images/regulator/pinout-regul.png)

![](.images/regulator/ht7533.jpg)

*Note: There will be a complete guide how to connect the regulator circuit of the kit.*


## IC

This componnent has whole dedicated [page](./processor-pinout.md).

### Symbol

![](.images/TSSOP20.png)


# Examples
