# Edit i3 config

In order to make a Signal desktop app make floating on startup you need to edit
the i3 config file.

```bash
$ nano .config/regolith/i3/config
```

Then copy and paste following line into the config file.

```
for_window [class="Signal"] floating enable
```

Any window with class `Signal` will start as floating.

*Note: It is possible to edit the desktop file and add `--class floating_window` but it did not work for me.*

# Reload

Reload the session with shortcut. Default shortcut is `MOD + SHIFT + R`.
