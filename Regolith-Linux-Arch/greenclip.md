# Download 

You can download greenclip from AUR.

```bash
$ yay -S rofi-greenclip
```

# Modyfiy i3 config

You have to spawn the daemon for greenclip to capture copied items. You can do taht in `i3 config`. 

```bash
$ nano .config/regolith/i3/config
```

Then copy and paste following lines at the end of this file.

```
###############################################################################
# Greenclip settings
###############################################################################

set_from_resource $greenclip.keybind greenclip.keybind Shift+v

exec greenclip daemon
bindsym $mod+$greenclip.keybind exec rofi -modi "clipboard:greenclip print" -show clipboard -run-command '{cmd}' -theme $(xrescat rofi.theme)
```

First line will get keybind from `Xresources`. Then the second line will spawn the daemon. 
The last line will bind the shortcut and then exec the rofi.

# Xresources

To change the keybind you have to create new Xresource in `~/.Xresources-regolith` file.

```bash 
$ nano .Xresources-regolith
```

Then copy and paste following line into this file.

```
greenclip.keybind: Shift+v
```

*Note: Default shortcut is MOD + SHIFT + V (same as windows).*

*Note: If no Xresource is set it shuld use the default keybind.*

# Reboot

You have to reboot to start the deamon.

```bash
$ reboot
```
