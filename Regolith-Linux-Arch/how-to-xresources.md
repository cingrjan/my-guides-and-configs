# How to overrride Xresource

There are Xresouces somewhere in `/etc/`. You can set Xresources in your home directiory.
Xresources file in your home directory has bigger priority, so it will set everything 
from in and then fill the rest from somewhere else.

```bash
$ nano ~/.Xresources-regolith
```

If this file does not exist just create new. 

# Xresource format

The Xresource is defined as `<name>: <value>`. 

```
i3-wm.client.focused.color.child_border: #00b800
```

Commenting can be done using `!` sign. For example

```
! This is a comment
```

You can also use `#include` and `#define` from C++ preprocesor.

# Where to find Xresources

You can find most Xresouces [here](https://regolith-linux.org/docs/reference/xresources/).
But some are missing and also you can define new one.

# New Xresouce

Just add the new Xresouce to the `~/.Xresouces-regolith` file. 

# How to check Xresouce

You can use command `xrescat`. This will return vaule for the Xresource.

```bash
$ xrescat i3-wm.binding.launcher.app
```

# Reload session

Each time you change Xresource you have to realod the session.
This can be dome by shortcut. Default keybind is `MOD + SHIFT + R`.

# Examples

- ## Window border color

You can set the windows border color by changing the value of 
`i3-wm.client.*.child_border`. You have to change the `*` for 
one of the four possilbe stated (given by i3).

To chage border of the active windows you can add following line 
to the `Xresouces-regolith` file.

```
i3-wm.client.focused.color.child_border: #00b800
```

- ## Gaps between windows

```
! Colors
i3-wm.gaps.inner.size: 0
i3-wm.gaps.outer.size: 0
```

- ## Binding

```
! Bindings
i3-wm.binding.launcher.app: x
i3-wm.binding.launcher.cmd: shift+x
i3-wm.binding.launcher.window: ctrl+x
```

- ## Define 

```
#define WORKSPACE_NAME(INDEX, FONT, COLOR, GLYPH) INDEX:<span font_desc=FONT> INDEX </span><span foreground=COLOR>GLYPH</span><span>  </span>
```

- ## Include

```
#include "/etc/regolith/styles/midnight/root"
```

