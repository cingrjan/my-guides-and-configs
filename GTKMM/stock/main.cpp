class TestWindow : public Gtk::Window {
    public:
        TestWindow();
        virtual ~TestWindow();

    protected:
        Gtk::Box main_box;

        Gtk::Stack main_stack;
        Gtk::StackSwitcher ss;

        Gtk::Label test_label;
        Gtk::Label another_label;

        Gtk::CenterBox box;
        Gtk::CenterBox boxx;
};

TestWindow::TestWindow() {
    test_label.set_text("test label");
    another_label.set_text("another test label");

    box.set_center_widget(test_label);
    boxx.set_center_widget(another_label);

    main_stack.add(box, "test", "test");
    main_stack.add(boxx, "another", "another");

    main_stack.set_expand(true);
    ss.set_stack(main_stack);

    main_box.set_expand(true);
    main_box.append(ss);
    main_box.append(main_stack);
    set_child(main_box);
}

TestWindow::~TestWindow() {

}

/*   IN MAIN
      TestWindow twin;
      app->add_window(twin);
      twin.show();
