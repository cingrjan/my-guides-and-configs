# MySQL official turorial

Official tutorrial [here](https://dev.mysql.com/doc/refman/8.0/en/linux-installation-debian.html).

# Download

I do not have GUI on my Debian machine, so I downleded it on my Arch machine and then copied the file via SSH.

You can download MySQL APT package [here](https://dev.mysql.com/downloads/repo/apt/).

When download completed you have to transfer the file. You can do this with command `scp`.

```bash
$ scp <source> <destination>
```

For example you can use following commant (assuming that you downloded in to Downloads folder.

```bash
$ scp Downloads/ user@10.0.0.50:/home/user
```

*Note: You have to change the `user` for your user name.*

# Install the package

You have to install the package. For this you can use `dpkg` command. In previous example 
I've copied the installation file into home directory of user.

```bash
$ sudo dpkg -i mysql-apt-config_*.deb
```

Then you have to update package information.

```bash
$ sudo apt-get update
```

# Install mysql-server

Now you can finally install MySQL server. You can do this by installing `mysql-server` package.

```bash
$ sudo apt install mysql-server
```

This will show you the installation wizard. Just go through it. `mysql.service` shloud start 
at the end od this installation.

## Check the status of MySQL

You can check the status of `mysql.service` with following command.

```bash
$ systemctl status mysql
```

## Start MySQL automaticaly

You can enable the `mysql.service`. This will start MySQL automaticaly when starting the machine.

```bash
$ sudo systemctl enable mysql
```

## Stop MySQL from starting automaticaly

You can diable the `mysql.service`. This will prevent MySQL from starting on startup.

```bash
$ sudo systemctl disable mysql
```

# Connect on localhost

On the machine with the `mysql-server` you can connect easily without no IP addres or anything else.
You can use `mysql` or `mycli`.

```bash
$ sudo mysql
```

# Connect via SSH

You have to create new user and set privileges for him.

## Create user

Fist let's create new user by following command.

```sql
mysql> create user 'user'@'ip_addr' identified by 'password';
```

*Note: You have to change the `user`, `ip_addr` and `password`.*

When you have a user you can grant privileges.

```mysql
mysql> GRANT ALL PRIVILEGES ON *.* TO 'user'@'ip_addr';
```

*Note: You have to change the `user` and `ip_addr`.*

*Note: In MySQL 8.0 `GRANT` will not create account automaticaly.*

## Connect

You can connect with following command.

```bash
$ mycli -h <destination>
```

For example 

```bash
$ mycli -h 10.0.0.50
```

You can also use `-u` to set the user and `-p` flag to pass the password.

```bash
$ mycli -h 10.0.0.50 -u user -p password
```

*Note: You have to install `mycli` first.*
